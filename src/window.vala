
namespace DemoApp {
	[GtkTemplate (ui = "/org/example/App/window.ui")]
	public class Window : Hdy.ApplicationWindow {
		[GtkChild]
		Gtk.Label label;

		public Window (Gtk.Application app) {
			Object (application: app);
		}
	}
}
